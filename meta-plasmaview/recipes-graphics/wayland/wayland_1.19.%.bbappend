FILESEXTRAPATHS:prepend := "${THISDIR}/${BPN}:"

SRC_URI += "\
    file://0001-Ported-KDE-wayland-patch.patch \
    "

SRC_URI:remove = "\
    file://0001-Consider-pkgconfig-sysroot-for-pkgdatadir.patch \
    "
