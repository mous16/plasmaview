#!/usr/bin/env bash

git config --global user.email "builder@none.not"
git config --global user.name "PlasmaView builder"
git config --global color.ui false

cd /build

ACTION="${1}"
shift 1

if [ "${ACTION}" = 'init' ]
then
	repo init -u "https://gitlab.com/mous16/plasmaview.git"

elif [ "${ACTION}" = 'build' ]
then
	repo sync
	
	source "setup.sh"

	bitbake -k core-image-minimal

elif [ "${ACTION}" = 'shell' ]
then
	YOCTO_DEVICE="${1}"
	shift 1

	repo sync
	
	source "setup.sh"
	
	bash 

fi

