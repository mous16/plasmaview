#!/bin/sh

while test -n "$1"; do
  case "$1" in
    "--help" | "-h")
      echo "Usage: . $0 [build directory]"
      return 0
      ;;
    *)
      BUILDDIRECTORY=$1
    ;;
  esac
  shift
done

THIS_SCRIPT="setup.sh"
if [ "$(basename -- $0)" = "${THIS_SCRIPT}" ]; then
    echo "Error: This script needs to be sourced. Please run as '. $0'"
    return 1
fi

BUILDDIRECTORY=${BUILDDIRECTORY:-plasmaview}

export TEMPLATECONF="${PWD}/sources/gitlab_mous16/plasmaview/meta-plasmaview/plasmaview.conf"
. sources/yoctoproject/poky/oe-init-build-env "${BUILDDIRECTORY}"

unset BUILDDIRECTORY
unset TEMPLATECONF

